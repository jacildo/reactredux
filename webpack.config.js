var webpack = require("webpack");
var path = require("path");

module.exports = {
	entry: [
		'script!jquery/dist/jquery.min.js',
		'script!bootstrap/dist/js/bootstrap.min.js',
		'swiper',
		'bootstrap/dist/css/bootstrap.css',
		'./app/app.jsx',
	],
	externals: {
		jquery: 'jQuery'
	},
	plugins: [
		new webpack.ProvidePlugin({
			'$': 'jquery',
			'jQuery': 'jquery'
		})
	],
	output: {
		path: __dirname,
		filename: './public/bundle.js'
	},
	resolve: {
		root: __dirname,
		modulesDirectories: [
			'node_modules',
			'./app/components',
			'./app/services'
		],
		alias: {
			actions: 'app/redux/actions/actions.jsx',
			applicationStyles: 'app/styles/app.scss',
			reducers: 'app/redux/reducers/reducers.jsx'
		},
		extensions: ['', '.js', '.jsx']
	},
	module: {
		loaders: [
			{
				loader: 'babel-loader',
				query: {
					presets: ['react', 'es2015', 'stage-0']
				},
				test: /\.jsx?$/,
				exclude: /(node_modules|bower_components)/
			},
			{
				loader: "style-loader!css-loader",
				test: /\.css$/
			},
			{
				loader : 'url-loader',
				test   : /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/
			}
		]
	},
	devtool: 'cheap-module-eval-source-map'
};
