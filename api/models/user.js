/*jslint node: true */

(function () {
    'use strict';

    var mongoose = require('mongoose'),
        User = new mongoose.Schema({
            username: {
                type: String,
                required: true
            },
            password: {
                type: String,
                required: true
            },
            email: {
                type: String,
                required: true,
                unique: true
            },
            type: {
                type: String,
                required: true
            }
        });

    module.exports = mongoose.model('User', User);

}());
