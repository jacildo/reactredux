/*jslint node: true */

(function () {
    'use strict';

    var mongoose = require('mongoose'),
        Comment  = require('../models/comment.js'),
        ObjectID = mongoose.Schema.Types.ObjectId,
        schema   = new mongoose.Schema({
            title: {
                type: String,
                required: true
            },
            body: {
                type: String
            },
            description: {
                type: String
            },
            slug: {
                type: String,
                required: true
            },
            comments : [
                {
                    type: ObjectID,
                    ref: "Comment"
                }
            ]

        });

    module.exports = mongoose.model('Blog', schema);
}());
