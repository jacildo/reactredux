/*jslint node: true */

(function () {
    'use strict';

    var mongoose = require('mongoose'),
        Comment  = require('../models/comment.js'),
        ObjectID = mongoose.Schema.Types.ObjectId,
        schema   = new mongoose.Schema({
            name: {
                type: String,
                required: true
            },
            description: {
                short: {
                    type: String,
    				required: true
                },
                full: {
                    type: String,
    				required: true
                }
            },
            rating: {
                type: Number,
                min: 0,
                max: 5
            },
			ingredients: [
				{
					name: {
						type: String,
						required: true
					},
					amount: {
						type: Number,
						required: true
					},
					measurement: {
						type: String,
						required: true
					}
				}
			],
            instructions: [
				{
	                type: String,
	                required: true
	            }
			],
            image: {
                url: {
                    type: String,
                    required: true
                },
                alt: {
                    type: String,
                    required: true
                }
            },
            author: {
                type: String,
                required: true
            },
            comments : [
                {
                    type: ObjectID,
                    ref: "Comment"
                }
            ]

        });

    module.exports = mongoose.model('Recipe', schema);
}());
