/*jslint node: true */

(function () {
    'use strict';

    var mongoose = require('mongoose'),
        ObjectID = mongoose.Schema.Types.ObjectId,
        schema   = new mongoose.Schema({
            message: {
                type: String,
                required: true
            },
            user: {
                type: ObjectID,
                required: true,
                ref: "User"
            },
            target: {
                type: ObjectID,
                required: false
                // target blog in comment section
                // figure out method for nested comments
            }

        });

    module.exports = mongoose.model('Comment', schema);
}());
