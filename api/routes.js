/*jslint node: true, nomen: true */

(function () {
    'use strict';

    // grab the nerd model we just created
    var path           = require('path');

    module.exports = function (app) {
        // server routes

        require('./routes/auth')(app); // import authentication routes first

        // import routes
        require('./routes/blog')(app);
        require('./routes/recipe')(app);

        // frontend routes
        app.use('*', function (req, res) {
            res.sendFile('public/index.html', {root: app.dirname}, function (err) {
                if (err) {
                    console.log(err);
                }
            });
        });

    };
}());
