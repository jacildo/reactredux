/*jslint node: true */

(function () {
    'use strict';

    var bodyParser      = require('body-parser'),
        methodOverride  = require('method-override'),
        path            = require('path'),
        port            = process.env.PORT || 3000,
        ipaddress       = process.env.IP || '127.0.0.1';


    module.exports = function (app) {

        // Parse body as JSON
        app.use(bodyParser.json());
        app.use(bodyParser.json({ type: 'application/vnd.api+json' }));

        app.use(bodyParser.urlencoded({ extended: true }));

        // override with the X-HTTP-Method-Override header in the request. simulate DELETE/PUT
        app.use(methodOverride('X-HTTP-Method-Override'));


        // secret key for encryption
        app.set("secret", "jackedsunsetcactusflower8519");

        // set directory (two levels above this file)
        /*jslint nomen: true */
        app.dirname = path.join(__dirname, '../', '../');
        app.staticdir = path.join(app.dirname, 'public');
        /*jslint nomen: false */

        app.start = function () {
            app.listen(port, ipaddress, function () {
                console.log('\nNode server started\n  date: %s\n  host: %s\n  port: %d',
                    Date(Date.now()),
                    ipaddress,
                    port);
            });
        };


    };
}());
