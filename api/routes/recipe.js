/*jslint node: true */

(function () {
    'use strict';

    var Recipe = require('../models/recipe.js');

    module.exports = function (app) {
        app.route('/api/recipes')
            .get(function (req, res) {
                Recipe.find({}, function (err, data) {
                    res.json(err ? 'Error reading recipes' : data);
                });
            })
            .post(function (req, res) {
                var recipe = new Recipe(req.body);

                recipe.save(function (err) {
                    res.json(err ? 'Error saving recipe\n' + err : 'New recipe created');
                });
            });

        app.route('/api/recipes/:id')
            .get(function (req, res) {
                // get all comments given a blog id
            })
            .post(function (req, res) {
                //get blog id, post comment to blog id,
                // or post to slug
                // by default, maybe comment should be disabled (and approved later)
            });
    };

}());
