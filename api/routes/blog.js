/*jslint node: true */

(function () {
    'use strict';

    var Blog = require('../models/blog.js');

    module.exports = function (app) {
        app.route('/api/blogs')
            .get(function (req, res) {
                Blog.find({}, function (err, data) {
                    res.json(err ? 'Error reading blogs' : data);
                });
            })
            .post(function (req, res) {
                // TODO: authenticate user (admin) then start sesssion
                // passport + (some session tool)
                var post = new Blog(req.body);

                post.save(function (err) {
                    res.json(err ? 'Error saving post\n' + err : 'Created new post');
                });
            });

        app.route('/api/blogs/:blogid/comments')
            .get(function (req, res) {
                // get all comments given a blog id
            })
            .post(function (req, res) {
                //get blog id, post comment to blog id,
                // or post to slug
                // by default, maybe comment should be disabled (and approved later)
            });
    };

}());
