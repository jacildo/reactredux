/*jslint node: true */

(function () {
    'use strict';

    var User = require('../models/user'),
        jwt  = require('jsonwebtoken'),
        express = require('express');

    function login(req, res, next) {
        console.log("login attempt");
        User.findOne({
            email: req.body.email
        }, function (err, user) {
            if (err) {
                throw err;
            }

            if (err || !req.body.password) {
                res.json({success: false, message: 'User not found.'});
            } else {
                if (user.password !== req.body.password) {
                    res.json({
                        success: false,
                        message: 'Authentication failed.'
                    });
                } else {
                    req.user = user;
                    next();
                }
            }
        });
    }

    module.exports = function (app) {
        var adminRoutes = express.Router();

        app.route('/api/login')
            .get(function (req, res) {
                // return user info
                var token = req.body.token || req.query.token || req.header['x-access-token'];

                jwt.verify(token, app.get("secret"), function (err, decoded) {
                    //change this: save cookie
                    res.json(decoded);
                });
            })
            .post(login, function (req, res) {
                var token = jwt.sign(req.user, app.get("secret"), {
                    expiresIn: "24h"
                });

                res.json("Login success");

            });

        app.route('/api/logout')
            .get(function (req, res) {
                res.redirectTo('/admin/login');
            });


        adminRoutes.authenticate = function (req, res, next) {
            var token = req.body.token || req.query.token || req.header['x-access-token'];

            if (token) {
                jwt.verify(token, app.get("secret"), function (err, decoded) {
                    if (err) {
                        return res.json({
                            sucess: false,
                            message: 'Authentication failed, invalid token.'
                        });
                    } else {
                        req.decoded = decoded;
                        next();
                    }
                });
            } else {
                res.status(403).send({
                    success: false,
                    message: 'No token provided.'
                });
            }
        };

        // everything within admin routes should be authenticated
        adminRoutes.use(function (req, res, next) {
            // authenticate
            console.log("authenticate");
            console.log(req.body.token);
            var token = req.body.token || req.query.token || req.header['x-access-token'];

            if (token) {
                jwt.verify(token, app.get("secret"), function (err, decoded) {
                    if (err) {
                        return res.json({
                            sucess: false,
                            message: 'Authentication failed, invalid token.'
                        });
                    } else {
                        req.decoded = decoded;
                        next();
                    }
                });
            } else {
                res.status(403).send({
                    success: false,
                    message: 'No token provided.'
                });
            }
        });



        adminRoutes.route('/users')
            .get(function (req, res) {
                User.find({}, function (err, data) {
                    if (err) {
                        res.json(err);
                    } else {
                        res.json(data);
                    }
                });
            });

        adminRoutes.route('/create')
            .post(function (req, res) {
                // TODO: handle duplicates first
                var user = new User(req.body);

                user.save(function (err) {
                    if (err) {
                        res.json(err);
                    } else {
                        res.json("success");
                    }
                });

            });

        adminRoutes.get('*', function (req, res) {
            res.sendStatus(400);
        });

        app.use('/api/admin', adminRoutes);
    };
}());

