// var express = require('express');
//
// var app = express();
//
// app.use(express.static('public'));
//
// app.listen(3000, function () {
//     console.log("Express listening at port 3000.")
// });

/*jslint node: true */

(function () {

    'use strict';

    // import modules
    var express        = require('express'),
        bodyParser     = require('body-parser'),
        methodOverride = require('method-override'),
        path           = require('path'),
        routes         = require('./api/routes.js'),
        app            = express();

    // load server config
    require('./api/config/server')(app);

    // initialize the database;
    require('./api/config/db')();

    // set directory for static files
    app.use(express["static"](app.staticdir));

    // add routes to the app
    require('./api/routes')(app);

    app.start();

}());
