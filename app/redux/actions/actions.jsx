export var addNewIngredient = (ingredient) => {
    return {
        type: 'ADD_NEW_INGREDIENT',
        ingredient
    }
};

export var addNewInstruction = (instruction) => {
    return {
        type: 'ADD_NEW_INSTRUCTION',
        instruction
    }
};

export var addNewRecipe = (recipe) => {
    return {
        type: 'ADD_NEW_RECIPE',
        recipe
    }
};

export var setRecipeSearchText = (text) => {
    return {
        type: 'SET_RECIPE_SEARCH_TEXT',
        text
    }
}
