
var {initialStates} = require('./initialStates.jsx');

export var newRecipeReducer = (state = initialStates.newRecipe, action) => {
	switch(action.type) {
		case 'ADD_NEW_INGREDIENT':
			return Object.assign({}, state, {
				ingredients: [
					...state.ingredients,
					action.ingredient
				]
			});
		case 'ADD_NEW_INSTRUCTION':
			return Object.assign({}, state, {
				ingredients: [
					...state.instructions,
					action.ingredient
				]
			});
		default:
			return state;
	}
}

export var recipeSearchTextReducer = (state = '', action) => {
	switch(action.type) {
		case 'SET_RECIPE_SEARCH_TEXT':
			return action.text;
		default:
			return state;
	}
}

export var recipesReducer = (state = initialStates.recipes, action) => {
	switch(action.type) {
		default:
			return state;
	}
}
