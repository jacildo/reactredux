export var initialStates = {
	newRecipe: {
		ingredients: [],
		instructions: []
	},
	recipes: [
		{
			name: "Rick's Intense Hard Boiled Eggs",
			description: {
				short: "There's nothing better than the classics. A bowl of hard boiled eggs to complement any meal.",
				full: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec semper risus eu neque sagittis pharetra. Mauris nec magna a magna lobortis viverra a vitae odio. Proin et libero sagittis, mattis est nec, faucibus risus. Nullam euismod metus vitae ligula vehicula, a laoreet nisl varius. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nulla mi nisl, convallis sit amet placerat et, fermentum rutrum augue. Duis nec purus sed lacus hendrerit aliquam eu eget risus. Morbi pharetra ipsum vitae nibh facilisis tempus. Cras in turpis non ligula tempor elementum et quis ligula. Mauris nec turpis nec lacus pellentesque ultricies auctor a augue. Integer consequat nisl rutrum, tempor neque sed, lacinia purus. Vestibulum finibus malesuada nulla, id condimentum nulla tempus ultrices. Cras ultricies quam felis, vitae malesuada nisl elementum nec. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas."
			},
			rating: 3,
			ingredients: [
				{
					name: "Large Eggs",
					amount: 5,
					measurement: "cups"
				},
				{
					name: "Bowl",
					amount: 1,
					measurement: "pcs"
				}
			],
			instructions: [
				"Boil the hell out of it for 7 minutes",
				"Drain and add lukewarm water to cool",
				"Serve those eggs"
			],
			image: {
				url: "/images/banner/bowl-of-eggs-min.jpg",
				alt: "A bowl of five eggs"
			},
			author: "Morty Smith",
			comments : []
		},
		{
			name: "Berry Oatmeal",
			description: {
				short: "A healthy meal for everyone to enjoy. Low in calories, rich in antioxidants, but still tastes great.",
				full: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ut tempus arcu. Donec at nunc vel urna mattis sodales eu a turpis. Proin vitae purus ut mauris lacinia sollicitudin sit amet et lacus. Suspendisse facilisis sagittis condimentum. Vestibulum tempus, urna vel commodo volutpat, nunc augue fermentum nunc, ut faucibus dolor sapien vitae massa. Integer mollis, risus et lacinia tristique, dui urna maximus ipsum, porttitor congue sapien ligula ut nisi. Maecenas vitae dolor id massa molestie ornare. Curabitur in accumsan lorem, quis viverra erat. Aliquam consequat elit at elit fringilla, sed consequat justo pellentesque. Integer id sodales sem. Duis pellentesque commodo mauris, eu egestas diam faucibus et. Aenean lacinia quam nec ex viverra ultrices nec vitae ipsum. Suspendisse risus massa, pretium quis massa viverra, sollicitudin rhoncus ex."
			},
			rating: 4,
			ingredients: [
				{
					name: "Mixed Fresh Berries",
					amount: 1,
					measurement: "cups"
				},
				{
					name: "Oatmeal",
					amount: 0.5,
					measurement: "cups"
				},
			],
			instructions: [
				"Wait for someone to cook oatmeal",
				"Scoop berries with hand, and throw them in the cooked oatmeal",
				"Claim oatmeal and eat it"
			],
			image: {
				url: "/images/banner/bowl-of-berries.jpg",
				alt: "A small bowl of mixed berries"
			},
			author: "Birdman",
			comments : []
		},
		{
			name: "Blueberry Cheesecake 2.0",
			description: {
				short: "This is a recipe passed down through many generations in my family. If you haven't tasted the perfect cheesecake yet, that's probably because you haven't tried this recipe yet.",
				full: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)."
			},
			rating: 4,
			ingredients: [
				{
					name: "All Purpose Flour",
					amount: 4,
					measurement: "cups"
				},
				{
					name: "Fresh Blueberries",
					amount: 2,
					measurement: "cups"
				},
				{
					name: "Large Eggs",
					amount: 3,
					measurement: "pcs"
				},
			],
			instructions: [
				"Combine ingredients",
				"Stir until there are no more lumps visible",
				"Bake at 400F for 30 minutes, flipping the cake halfway through"
			],
			image: {
				url: "/images/banner/blueberry-cheese-cake-min.jpg",
				alt: "A sliced pan of blueberry cheesecake"
			},
			author: "Bob Smith",
			comments : []
		},
		{
			name: "Aunt Jessica's Carrot Cake",
			description: {
				short: "I'm not even related to Jessica, but everyone calls her Aunt Jessica. Anyway, I happened to be in a party she threw, and I saw a paper with scribbles labeled Carrot Cake",
				full: "Fusce risus ex, pharetra eu convallis in, blandit eget purus. Ut interdum porttitor tortor, eu sagittis felis tincidunt ac. Proin sed dictum augue, sit amet convallis magna. Aenean gravida, mauris viverra suscipit dictum, felis arcu molestie lacus, tincidunt scelerisque libero orci at tellus. Nullam cursus non est quis rutrum. In sapien dolor, imperdiet non volutpat nec, tempor ac sapien. Integer cursus nisi non interdum venenatis. Phasellus pellentesque ornare libero, at lacinia velit ornare quis. Praesent ac molestie dui, sed suscipit ex."
			},
			rating: 5,
			ingredients: [
				{
					name: "All Purpose Flour",
					amount: 4,
					measurement: "cups"
				},
				{
					name: "Large Carrots",
					amount: 1,
					measurement: "pc"
				},
				{
					name: "Ammonium Nitrate",
					amount: 42,
					measurement: "mg"
				},
			],
			instructions: [
				"Buy carrot cake from grocery",
				"Remove from package, then serve over a fancy platter",
				"Ammonium Nitrate to taste"
			],
			image: {
				url: "/images/banner/loaf-of-bread-min.jpg",
				alt: "A loaf of bread on a wooden chopping board"
			},
			author: "Professor X",
			comments : []
		},
		{
			name: "Raw Meat",
			description: {
				short: "My wife and I don't trust any manufactured product. Not even stoves or charcoal. This is why we eat our meat raw, although I have my suspicion that the meats they sell here are artificial as well.",
				full: "Curabitur eget est varius, condimentum diam id, egestas tellus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi venenatis ullamcorper lectus, ac euismod nibh maximus et. Nam semper accumsan auctor. Ut mattis, felis a sodales rhoncus, mi nisl ultricies diam, vitae iaculis velit nulla vitae ipsum. Curabitur at elit congue tellus rhoncus eleifend. In quis ipsum tortor. Praesent lorem lacus, fringilla sed volutpat ac, mollis dapibus tellus. In eget rutrum orci, ac iaculis turpis. Donec et urna eros. Nulla eleifend ex diam, ac sagittis eros euismod et. Aenean urna nisi, dignissim at dolor non, pellentesque efficitur diam. Aenean finibus placerat turpis. Vivamus accumsan euismod pulvinar. Donec scelerisque mi tellus, eget egestas felis sagittis nec. Quisque mollis turpis nec diam bibendum, vel commodo lectus tincidunt."
			},
			rating: 1,
			ingredients: [
				{
					name: "Meat",
					amount: 1,
					measurement: "slice"
				},
			],
			instructions: [
				"Eat the meat",
			],
			image: {
				url: "/images/banner/raw-slice-of-meat-min.jpg",
				alt: "A slice of meat with herbs around it"
			},
			author: "Ricky Bobby",
			comments : []
		},
		{
			name: "Garlic-White Wine Mussels",
			description: {
				short: "An elegant, cheap, and delicious dish that everyone would love. A must try for people, especially those who live seafood like I do.",
				full: "Ut quis euismod augue, nec ultricies est. Donec leo sem, faucibus id metus vel, dignissim tempor eros. Curabitur nec volutpat tellus. Vestibulum condimentum ante non gravida rutrum. Morbi pharetra ligula vitae finibus scelerisque. Ut placerat maximus sollicitudin. Etiam at ullamcorper elit. Proin rutrum justo mi, et mattis sem porttitor eu. Nullam pretium porta massa, non gravida velit laoreet ut. Etiam cursus diam vitae elementum pulvinar. Integer placerat interdum leo tristique ultricies. Praesent ut laoreet lacus."
			},
			rating: 5,
			ingredients: [
				{
					name: "Mussels",
					amount: 2,
					measurement: "lbs"
				},
				{
					name: "White Wine or Cooking Wine",
					amount: 1,
					measurement: "cup"
				},
				{
					name: "White Wine",
					amount: 1,
					measurement: "glass"
				},
				{
					name: "Spring Onions",
					amount: 3,
					measurement: "stalks"
				},
				{
					name: "Garlic",
					amount: 1,
					measurement: "head"
				},
				{
					name: "Olive Oil",
					amount: 3,
					measurement: "tbsp"
				},
			],
			instructions: [
				"In a sauce pan, saute chopped garlic with the olive oil until light brown",
				"Add the cup of white wine or cooking wine and let the mixture boil",
				"Throw the mussels in the pan",
				"Simmer for 5 minutes while consuming the glass of white wine",
				"Serve hot"
			],
			image: {
				url: "/images/banner/shellfish-min.jpg",
				alt: "Neatly arranged open shellfish"
			},
			author: "Brian Jacildo",
			comments : []
		}

	]

}
