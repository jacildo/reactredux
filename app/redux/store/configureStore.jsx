var redux = require('redux');
var {newRecipeReducer, recipeSearchTextReducer, recipesReducer} = require('reducers');

export var configure = (initialState = {}) => {
  var reducer = redux.combineReducers({
      newRecipe: newRecipeReducer,
      recipeSearchText: recipeSearchTextReducer,
      recipes: recipesReducer
  });

  var store = redux.createStore(reducer, initialState, redux.compose(
    window.devToolsExtension ? window.devToolsExtension() : f => f
  ));

  return store;
};
