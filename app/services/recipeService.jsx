var $ = require('jquery');

module.exports = {
  filterRecipes: function (recipes, query) {
    var filtered = recipes;

    filtered = filtered.filter((recipe) => {
      var name = recipe.name.toLowerCase();
      return query.length === 0 || name.indexOf(query) > -1;
    });

    return filtered;
  }
};
