var React = require('react');

var About = (props) => {
	return (
		<div className="container">
			<div className="col-md-8">
				<h1>About page</h1>
				<p>
					This site uses the following technologies:
				</p>
				<ul>
					<li>React</li>
					<li>Redux</li>
					<li>Node</li>
					<li>MongoDB</li>
				</ul>
			</div>
		</div>
	)
}

module.exports = About;
