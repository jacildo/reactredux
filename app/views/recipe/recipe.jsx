var React = require('react');
var {connect} = require('react-redux');
var actions = require('actions');
var recipeService = require('recipeService');

export var Recipe = React.createClass({
	componentDidMount: function () {
		// slider on top of page
		var pagination = (document.getElementsByClassName('swiper-slide').length > 1) ? true : false;
		var mySwiper = new Swiper('.swiper-container', {
			loop: pagination,
			pagination: '.swiper-pagination',
			paginationClickable: true,
			autoplay: 10000
		})
	},
	render: function () {
		var {recipes, recipeSearchText, dispatch} = this.props;
		var renderRecipes = () => {
			if(recipes.length === 0) {
				<p className="">No recipes available</p>
			}

			return recipeService.filterRecipes(recipes, recipeSearchText).map((recipe) => {
				return (
					<div key={recipe.id} className="col-md-4">
						<div className="thumbnail">
							<img src={recipe.image.url} alt={recipe.image.alt} />
							<div className="caption">
								<h6>{recipe.name}</h6>
								<p className="text-description">{recipe.description.short}</p>
							</div>
						</div>
					</div>
				);
			});
		}

		return (
			<div>
				<div className="page-banner">
					<div className="swiper-container">
			            <div className="swiper-wrapper">
		                    <div className="swiper-slide testimonial">
								<img className="img-responsive" src="/images/banner/blueberry-cheese-cake-min.jpg"></img>
		                    </div>
							<div className="swiper-slide testimonial">
								<img className="img-responsive" src="/images/banner/bowl-of-berries.jpg"></img>
		                    </div>
							<div className="swiper-slide testimonial">
								<img className="img-responsive" src="/images/banner/bowl-of-eggs-min.jpg"></img>
		                    </div>
							<div className="swiper-slide testimonial">
								<img className="img-responsive" src="/images/banner/loaf-of-bread-min.jpg"></img>
		                    </div>
							<div className="swiper-slide testimonial">
								<img className="img-responsive" src="/images/banner/raw-slice-of-meat-min.jpg"></img>
		                    </div>
							<div className="swiper-slide testimonial">
								<img className="img-responsive" src="/images/banner/shellfish-min.jpg"></img>
		                    </div>
			            </div>
			        </div>
					<div className="container">
						<div className="page-banner-footer">
							<div className="swiper-pagination"></div>
							<input className="form-control input-recipe-filter" type="text" placeholder="Search recipes" ref="recipeSearchText" onChange={() => {
									var recipeSearchText = this.refs.recipeSearchText.value;
									dispatch(actions.setRecipeSearchText(recipeSearchText));
								}
							}></input>
						</div>
					</div>
				</div>
				<div className="container content">
					<div className="col-md-9">
						<div className="featured-recipe card">
							<div className="featured-recipe-description">
								<h4>RECIPE OF THE DAY</h4>
								<p className="text-description">This is a recipe passed down through many generations in my family. If you haven't tasted the perfect cheesecake yet, that's probably because you haven't tried this recipe yet.</p>
							</div>
							<div className="featured-recipe-preview">
								<img className="img-responsive" src="/images/banner/blueberry-cheese-cake-min.jpg" />
								<label>Blueberry Cheesecake 2.0</label>
							</div>
						</div>
						<div className="row">
							{renderRecipes()}
						</div>
					</div>
					<div className="col-md-3">
						<h2>TEST </h2>
					</div>
				</div>
			</div>
		)
	}
});

export default connect(
	(state) => {
		return state;
	}
)(Recipe);
