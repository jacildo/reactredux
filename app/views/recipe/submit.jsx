var React = require('react');
var {connect} = require('react-redux');
var actions = require('actions');

export var SubmitRecipe = React.createClass({
	handleSubmit: function (e) {
		var {dispatch} = this.props;
		recipe = {
			...recipe,
			name: this.refs.name.value,
			author: this.refs.author.value,
			image: {
				url: this.refs.imageUrl.value
			},
			description: {
				short: this.refs.shortDescription.value,
				full: this.refs.fullDescription.value
			}
		};

		// dispatch recipe
	},
	handleAddIngredient: function (e) {
		e.preventDefault();
		var {dispatch} = this.props;
		var name = this.refs.newIngredientName.value;
		var measurement = this.refs.newIngredientMeasurement.value;
		var amount = this.refs.newIngredientAmount.value;

		// there has to be a better way to do this
		if(name.length === 0) {
			this.refs.newIngredientName.focus();
		} else if(measurement.length === 0) {
			this.refs.newIngredientMeasurement.focus();
		} else if(amount.length === 0) {
			this.refs.newIngredientAmount.focus();
		} else {
			var ingredient = {name, measurement, amount};

			this.refs.newIngredientName.value = "";
			this.refs.newIngredientMeasurement.value = "";
			this.refs.newIngredientAmount.value = "";

			dispatch(actions.addNewIngredient(ingredient));
		}
	},
    handleAddInstruction: function (){

	},
	render: function () {
		var {newIngredient} = this.props;
		console.log(newIngredient);

		var renderIngredients: () => {

		};
		return (
			<div className="container content">
				<form onSubmit={this.handleSubmit}>
					<h5>Describe your recipe: </h5>

					<div className="form-group col-md-6">
						<input className="form-control" type="text" ref="name" maxLength="200" placeholder="Recipe name" required/>
					</div>

					<div className="form-group col-md-6">
						<input className="form-control" type="text" ref="author" maxLength="100" placeholder="Author name" required/>
					</div>

					<div className="form-group col-md-6">
						<input className="form-control" type="text" ref="imageUrl" placeholder="Image url" required/>
					</div>

					<div className="form-group col-md-12">
						<textarea className="form-control" rows="3" ref="shortDescription" maxLength="200" placeholder="A short introduction of your recipe to catch peoples' interest."></textarea>
					</div>

					<div className="form-group col-md-12">
						<textarea className="form-control" rows="5" ref="fullDescription" placeholder="Now tell us all about your amazing recipe."></textarea>
					</div>

					<div className="clearfix"></div>

					<br />
					<h5>Ingredients: </h5>
					<hr />
					<input type="text" ref="newIngredientName" placeholder="Add an ingredient" />
					<input type="text" ref="newIngredientMeasurement" placeholder="Add an ingredient" />
					<input type="text" ref="newIngredientAmount" placeholder="Add an ingredient" />
					<a className="btn btn-sm btn-success" onClick={this.handleAddIngredient} >Add</a>

					<br />
					<h5>Instructions: </h5>
					<hr />
					<input type="text" ref="newInstruction" placeholder="Add an instruction" />
					<a className="btn btn-sm btn-success" onClick={this.handleAddInstruction} >Add</a>

					<input className="btn btn-success" type="submit" value="Submit" />

				</form>
			</div>
		)
	}
});

export default connect(
	(state) => {
		return state;
	}
)(SubmitRecipe);

// export default connect(
// 	(state) => {
// 		return {
// 			name : state.newRecipe.name,
// 			author: state.newRecipe.author,
// 			'image.url' : state.newRecipe.image.url,
// 			'description.short' : state.newRecipe.description.short,
// 			'description.full' : state.newRecipe.description.full
// 		}
// 	}
// )(SubmitRecipe);
