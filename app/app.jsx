var React = require('react');
var ReactDOM = require('react-dom');
var {Provider} = require('react-redux');
var {Route, Router, IndexRoute, browserHistory } = require('react-router');

// handle redux
var actions = require('actions');
var store = require('./redux/store/configureStore.jsx').configure();

// main component
var Main    = require('MainComponent');

// default views
var Index 	= require('./views/default/index.jsx');
var About 	= require('./views/default/about.jsx');
var Contact = require('./views/default/contact.jsx');

// recipes views
import Recipe from './views/recipe/recipe.jsx';
import SubmitRecipe from './views/recipe/submit.jsx';

// load application styles
require('style!css!sass!applicationStyles');

ReactDOM.render(
	<Provider store={store} key="provider">
		<Router history={browserHistory}>
			<Route path="/" component={Main}>
				<IndexRoute  			component={Index}/>
				<Route path="about" 	component={About}/>
				<Route path="contact" 	component={Contact}/>

				<Route path="recipes" component={Recipe}/>
				<Route path="submit-recipe" component={SubmitRecipe}/>
			</Route>
		</Router>
	</Provider>,
	document.getElementById('app')
);
