var React = require('react');
var {Link, IndexLink} = require('react-router');

var Navbar = (props) => {
	return (
        <nav className="navbar navbar-inverse">
            <div className="container container-fluid">
                <div className="navbar-header">
                    <button type="button" className="navbar-toggle pull-right" data-toggle="collapse" data-target=".navbar-collapse">
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                    </button>
                    <a className="navbar-brand" href="/">React-Redux</a>
                </div>
                <div className="collapse navbar-collapse">
                    <ul className="nav navbar-nav navbar-right">
                        <li><IndexLink to="/" activeClassName="active">Home</IndexLink></li>
                        <li><Link to="/about" activeClassName="active">About</Link></li>
						<li><Link to="/recipes" activeClassName="active">Recipes</Link></li>
            			<li><Link to="/contact" activeClassName="active">Contact</Link></li>
                    </ul>
                </div>
            </div>
        </nav>
	);
}

module.exports = Navbar;
